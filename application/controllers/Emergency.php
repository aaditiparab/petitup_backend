<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Emergency extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Emergency_model');
    }

    /* START RENDER PAGE */

    function index() {
        $data = array();
        $this->render('emergency', $data);
    }

    /* END RENDER PAGE */

    /* START READ OPERATION */

    function getEmergencyListing() {        
        $offset = $this->input->post('offset');
        $search = $this->input->post('search');
        $status = $this->input->post('status');
        $fromDate = $this->input->post('fromDate');
        $toDate = $this->input->post('toDate');
        $result = $this->Emergency_model->getEmergencyListing($offset, $search, $status, $fromDate, $toDate);
        echo json_encode($result);
    }

    /* END READ OPERATION */

    /* START DELETE OPERATION*/

    function deleteEmergency(){
        $id = $this->input->post('id');
        $result = $this->Emergency_model->deleteEmergency($id);
        echo json_encode($result);        
    }

    /* END DELETE OPERATION*/

    /* START UPDATE OPERATION */

    function changeEmergencyStatus(){
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        $result = $this->Emergency_model->updateEmergencyStatus($id, $status);
        echo json_encode($result);
    }

    /* END UPDATE OPERATION */

}