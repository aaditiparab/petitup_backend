<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Pets extends Base_Controller {

    function __construct() {
        parent::__construct();        
        $this->load->model('Pets_model');
    }

    function getCategory(){
    	$result = $this->Pets_model->getCategory();
        echo json_encode($result);
    }
}