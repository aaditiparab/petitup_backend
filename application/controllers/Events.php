<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Events extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Events_model');
    }

    function index() {
        $data = array();
        $this->render('events', $data);
    }

    /**
     * Fetch and bind the events data
     */
    function getEventsDetails() {
        $offset = $this->input->post('offset');
        $result = $this->Events_model->get_Events_Details($offset);
        echo json_encode($result);
    }
    
    /**
     * create_update_event
     * 
     * @param string $question this question entered by admin
     * @return json return status and success message
     */
    function create_update_event() {
        $eventid = $this->input->post('eventid');
        $eventname = $this->input->post('eventname');
        $eventdesc = $this->input->post('eventdesc');
        $eventlocation = $this->input->post('eventlocation');
        $eventcity = $this->input->post('eventcity');
        $startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');
        
        if ($question == "") {
            echo json_encode(array("status" => FALSE, "message" => "Please enter question"));
            exit();
        }

        $result = $this->Events_model->createUpdateEvent();
        echo json_encode(array("status" => TRUE, "message" => $result));
        exit();
    }
    
    /**
     * Delete Events
     */
    function deleteEvents() {
        $eventid = $this->input->post('eventid');
        $result = $this->Events_model->delete_Events($eventid);
        echo json_encode($result);
    }

}
