<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {

        $sessiondata = $this->session->userdata('logged_in');
        if (!empty($sessiondata)) {
            redirect('Adoption/');
        } else {
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[10]|callback_authenticate');
            if ($this->form_validation->run($this) === FALSE) {
                $this->load->view('login');
            } else {
                redirect('Adoption/');
            }
        }
    }

    public function authenticate() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        if ($password == "") {
            $this->form_validation->set_message('authenticate', 'The %s field is required.');
            return FALSE;
        }
        $this->db->select('id,name');
        $this->db->from('admin');
        $this->db->where(array("email" => $username, "password" => md5($password)));
        $result = $this->db->get();

        if ($result->num_rows() > 0) {
            $data = $result->result_array();
            $session_array = array(
                "userid" => $data[0]['id'],
                "name" => $data[0]['name'],
            );
            $this->session->set_userdata('logged_in', $session_array);
            return TRUE;
        } else {
            $this->form_validation->set_message('authenticate', 'Invalid Username or Password');
            return FALSE;
        }
    }

    public function logout() {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('Login');
    }

}
