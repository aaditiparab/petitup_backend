<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Tips extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Tips_model');
    }

    /**
     * Load Tips view
     */
    function index() {
        $data = array();
        $this->render('tips', $data);
    }

    /**
     * Fetch and bind the tips data
     */
    function getTipsDetails() {
        $offset = $this->input->post('offset');
        $extra = $this->input->post('extra');

        $result = $this->Tips_model->getTipsDetails($offset, $extra);
        echo json_encode($result);
    }

    /**
     * create_update_tips
     * 
     * @param string $question this question entered by admin
     * @return json return status and success message
     */
    function create_update_tips() {
        $quesid = $this->input->post('quesid');
        $question = $this->input->post('question');

        if ($question == "") {
            echo json_encode(array("status" => FALSE, "message" => "Please enter question"));
            exit();
        }

        $result = $this->Tips_model->createUpdateTips($quesid, $question);
        echo json_encode(array("status" => TRUE, "message" => $result));
        exit();
    }

    /**
     * Load Tips Answers view
     */
    function tipsAnswers() {
        $data = array();
        $this->render('tips_answers', $data);
    }

    /**
     * Fetch and bind the tips data
     */
    function getTipsAnswers() {
        $quesid = $this->input->post('quesid');
        $offset = $this->input->post('offset');
        $extra = $this->input->post('extra');
        $result = $this->Tips_model->getTipsAnswers($quesid, $offset, $extra);
        echo json_encode($result);
    }

    /**
     * Delete Tips
     */
    function deleteTips() {
        $quesid = $this->input->post('quesid');
        $result = $this->Tips_model->delete_tips($quesid);
        echo json_encode($result);
    }

    function deleteTipsAnswer() {
        $ansid = $this->input->post('ansid');
        $result = $this->Tips_model->delete_tips_answer($ansid);
        echo json_encode($result);
    }

}
