<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Adoption extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Adoption_model');
        $this->load->model('Pets_model');
    }


    /* START RENDER PAGE */

    function index() {
        $data = array();
        $this->render('adoption', $data);
    }

    function adoptionpets() {
        $data = array();
        $this->render('adoption_pets', $data);
    }

    /* END RENDER PAGE */

    /* START READ OPERATION */

    function getOwnerDetails() {
        $id = $this->input->post('id');
        $offset = $this->input->post('offset');
        $search = $this->input->post('search');
        $gender = $this->input->post('gender');
        $fromDate = $this->input->post('fromDate');
        $toDate = $this->input->post('toDate');
        $result = $this->Adoption_model->getOwnerDetails($id, $offset, $search, $gender, $fromDate, $toDate);        
        echo json_encode($result);
    }

    function getPetDetails(){

        $id = $this->input->post('id');        
        $offset = $this->input->post('offset');
        $search = $this->input->post('search');
        $gender = $this->input->post('gender');
        $fromDate = $this->input->post('fromDate');
        $toDate = $this->input->post('toDate');
        
        $result = $this->Adoption_model->getOwnerPetDetails($id, $offset, $search, $gender, $fromDate, $toDate);
        echo json_encode($result);
    }

    function getFavoritePets(){

        $id = $this->input->post('id');
        $result = $this->Adoption_model->getFavoritePets($id);
        echo json_encode($result);
    }

    /* END READ OPERATION */

    /* START DELETE OPERATION */

    function deleteAdoptionOwner(){
        $id = $this->input->post('id');
        $result = $this->Adoption_model->deleteAdoptionOwner($id);
        echo json_encode($result);
    }

    function deleteAdoptionPet(){
        $id = $this->input->post('id');
        $result = $this->Adoption_model->deleteAdoptionPet($id);
        echo json_encode($result);
    }

    /* END DELETE OPERATION */    

}
