<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DateMate extends Base_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('DateMate_model');
    }


    /* START RENDER PAGE */

    function index() {
        $data = array();
        $this->render('datemate', $data);
    }

    function adoptionpets() {
        $data = array();
        $this->render('datemate_pets', $data);
    }

    /* END RENDER PAGE */

    /* START READ OPERATION */

    function getOwnerDetails() {
        $id = $this->input->post('id');
        $offset = $this->input->post('offset');
        $search = $this->input->post('search');
        $result = $this->DateMate_model->getOwnerDetails($id, $offset, $search);
        echo json_encode($result);
    }

    function getPetDetails(){

        $id = $this->input->post('id');        
        $offset = $this->input->post('offset');
        $result = $this->DateMate_model->getOwnerPetDetails($id, $offset);                
        echo json_encode($result);
    }

    function getFavoritePets(){

        $id = $this->input->post('id');
        $result = $this->DateMate_model->getFavoritePets($id);
        echo json_encode($result);
    }

    /* END READ OPERATION */

    /* START DELETE OPERATION */

    function deleteAdoptionOwner(){
        $id = $this->input->post('id');
        $result = $this->DateMate_model->deleteAdoptionOwner($id);
        echo json_encode($result);
    }

    function deleteAdoptionPet(){
        $id = $this->input->post('id');
        $result = $this->DateMate_model->deleteAdoptionPet($id);
        echo json_encode($result);
    }

    /* END DELETE OPERATION */    

}
