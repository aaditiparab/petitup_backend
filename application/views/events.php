<!-- PAGE CONTENT -->
<div class="page-content">

    <!-- START X-NAVIGATION VERTICAL -->
    <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
        <!-- TOGGLE NAVIGATION -->
        <li class="xn-icon-button">
            <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
        </li>
        <!-- END TOGGLE NAVIGATION -->
        <!-- SEARCH -->
        <li class="xn-search">
            <form role="form">
                <input type="text" name="search" placeholder="Search..."/>
            </form>
        </li>   
        <!-- END SEARCH -->
        <!-- SIGN OUT -->
        <li class="xn-icon-button pull-right">
            <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
        </li>
        <!-- END SIGN OUT -->
    </ul>
    <!-- END X-NAVIGATION VERTICAL -->                     

    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="Dashboard">Dashboard</a></li>                    
        <li class="active">Events</li>
    </ul>
    <!-- END BREADCRUMB -->                

    <div class="page-title">                    
        <h2><span class="fa fa-arrow-circle-o-left"></span> Events</h2>
    </div>                   

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <!-- START SIMPLE DATATABLE -->
                <div class="panel panel-default tips-panel">
                    <div class="panel-heading">                                
                        <h3 class="panel-title">Events Details</h3>
                        <!--<ul class="panel-controls" style="margin-top: 2px;">
                            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>                                       
                        </ul>-->
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Event</th>
                                    <th>User</th>
                                    <th>Description</th>
                                    <th>Location</th>
                                    <th>City</th>
                                    <th>Attendance</th>
                                    <th class="text-center">Start Date</th>
                                    <th class="text-center">End Date</th>
                                    <th class="text-center">Created Date</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody class="events-listing">
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-default">
                            <a href="#" class="load-more-events" style="text-decoration: none;">Load More</a>
                        </button>
                    </div>
                </div>
                <!-- END SIMPLE DATATABLE -->

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->                
</div>
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<!-- START DELETE QUESTION MODAL-->
<div id="cnf_delete_events" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this event?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" name="btn_del_evnt_cnf" id="btn_del_evnt_cnf">Confirm</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>
</div>
<!-- END DELETE QUESTION MODAL-->

<script>
    $(function () {
        var offset = 0;
        getEventsDetails(offset);
    });
</script>