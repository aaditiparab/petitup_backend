<!-- PAGE CONTENT -->
<div class="page-content">

    <!-- START X-NAVIGATION VERTICAL -->
    <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
        <!-- TOGGLE NAVIGATION -->
        <li class="xn-icon-button">
            <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
        </li>
        <!-- END TOGGLE NAVIGATION -->
        <!-- SEARCH -->
        <li class="xn-search">
            <form role="form">
                <input type="text" name="search" placeholder="Search..."/>
            </form>
        </li>   
        <!-- END SEARCH -->
        <!-- SIGN OUT -->
        <li class="xn-icon-button pull-right">
            <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
        </li>
        <!-- END SIGN OUT -->
    </ul>
    <!-- END X-NAVIGATION VERTICAL -->                     

    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="Dashboard">Dashboard</a></li>                    
        <li class="active">Adoption</li>
    </ul>
    <!-- END BREADCRUMB -->                

    <div class="page-title">                    
        <h2>Adoption Listing</h2>
    </div>                   

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default adopt-panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pet Owner Details</h3>
                    </div>
                    <div class="panel-body">
                        <!-- Filter Start -->                        
                        <div class="block">
                            <div class="form-group">
                            <label class="col-md-1 col-sm-offset-2 control-label">Search</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="Search by Name / Location / Description" id="adp-search">
                                </div>
                            <label class="col-md-1 control-label">Gender</label>
                                <div class="col-md-3">
                                    <select id="ddlAdpOwnerGender" class="form-control dropdown">
                                        <option value="">Select</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>
                                </div>
                            </div>                            
                        </div>
                        <div class="block">
                            <div class="form-group">
                                <label class="col-md-1 col-sm-offset-2 control-label">From Date</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control datepicker" id="adp-frm">
                                </div>
                                <label class="col-md-1 control-label">To Date</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control datepicker" id="adp-to">
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-primary active" id="adp-date-filter">Search</button>
                                </div>
                            </div>                            
                        </div>                        
                        <!-- Filter End -->
                        <!-- Listing start -->
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th width="200px">Location</th>
                                    <th width="200px">Description</th>
                                    <th>Pets</th>                                    
                                    <th>Interests</th>
                                    <th>Created Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody class="adoption-listing">
                            </tbody>
                        </table>
                        <!-- Listing end -->
                    </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-default">
                            <a href="#" class="load-more-adoption" style="text-decoration: none;">Load More</a>
                        </button>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->                
</div>            
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<!-- START DELETE ADOPTION OWNER MODAL-->
<div id="cnf_delete_adp_owner" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this owner?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" name="btn_del_cnf" id="btn_del_adp_owner_cnf">Confirm</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>
</div>
<!-- END DELETE QUESTION MODAL-->

<!-- START OWNER FAVORITES MODAL-->
<div id="cnf_pets_interest" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Short listed Pets by User</h4>
            </div>
            <div class="modal-body">
                <table class="table no-border">
                    <thead>
                        <tr>
                            <th>Owner Name</th>
                            <th>Owner Image</th>
                            <th>Pet Name</th>
                            <th>Pet Image</th>                                    
                        </tr>
                    </thead>
                    <tbody class="adp-fav-listing">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">                
                <button type="button" class="btn btn-danger" data-dismiss="modal">CANCEL</button>
            </div>
        </div>

    </div>
</div>
<!-- END OWNER FAVORITES MODAL-->

<script>
    $(function () {
        var offset = 0;
        var id = urlid[urlid.length - 1];        
        getAdoptionOwnerDetails('', '', id, offset);
    });
</script>