<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>Petitup Admin</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->
        <style>
            .error{
                color: #FFFFFF;
            }
        </style>
    </head>
    <body>

        <div class="login-container">

            <div class="login-box animated fadeInDown">
                <!--<div class="login-logo"></div>-->
                <div class="login-body">
                    <?php
//                    if (validation_errors()) {
//                        echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
//                    }
                    ?>
                    <div class="login-title"><strong>Welcome</strong>, Please login</div>
                    <form action="Login" class="form-horizontal" method="post">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="text" name="username" id="username" class="form-control" placeholder="Username" value="<?= set_value('username'); ?>"/>
                                <span class="error"><?= form_error('username'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
                                <span class="error"><?= form_error('password'); ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <!--<div class="col-md-6">
                                <a href="#" class="btn btn-link btn-block">Forgot your password?</a>
                            </div>-->
                            <div class="col-md-6 pull-right">
                                <button type="submit" class="btn btn-info btn-block">Log In</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!--<div class="login-footer">
                    <div class="pull-left">
                        &copy; 2014 AppName
                    </div>
                    <div class="pull-right">
                        <a href="#">About</a> |
                        <a href="#">Privacy</a> |
                        <a href="#">Contact Us</a>
                    </div>
                </div>-->
            </div>

        </div>

    </body>
</html>






