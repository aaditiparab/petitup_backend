<!-- PAGE CONTENT -->
<div class="page-content">
    <!-- START X-NAVIGATION VERTICAL -->
    <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
        <!-- TOGGLE NAVIGATION -->
        <li class="xn-icon-button">
            <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
        </li>
        <!-- END TOGGLE NAVIGATION -->
        <!-- SEARCH -->
        <li class="xn-search">
            <form role="form">
                <input type="text" name="search" placeholder="Search..."/>
            </form>
        </li>   
        <!-- END SEARCH -->
        <!-- SIGN OUT -->
        <li class="xn-icon-button pull-right">
            <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
        </li>
        <!-- END SIGN OUT -->
    </ul>
    <!-- END X-NAVIGATION VERTICAL -->
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="Dashboard">Dashboard</a></li>                    
        <li class="active">Emergency</li>
    </ul>
    <!-- END BREADCRUMB -->

    <!-- PAGE CONTENT WRAPPER -->    
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default adopt-panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Emergency Details</h3>
                    </div>
                    <div class="panel-body">                        
                        <!-- Filter Start -->                        
                        <div class="block">
                            <div class="form-group">
                            <label class="col-md-1 col-sm-offset-2 control-label">Search</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="Search by Emergency/ Name / Location" id="emr-search">
                                </div>
                            <label class="col-md-1 control-label">Status</label>
                                <div class="col-md-3">
                                    <select id="ddlEmergencyStatus" class="form-control dropdown">
                                        <option value="">Select</option>
                                        <option value="0">Pending</option>
                                        <option value="1">Closed</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="block">
                            <div class="form-group">
                                <label class="col-md-1 col-sm-offset-2 control-label">From Date</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control datepicker" id="emr-frm">
                                </div>
                                <label class="col-md-1 control-label">To Date</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control datepicker" id="emr-to">
                                </div>
                                <div class="col-md-1">
                                    <button type="button" class="btn btn-primary active" id="emr-date-filter">Search</button>
                                </div>
                            </div>                            
                        </div>
                        <!-- Filter End -->
                        <!-- Listing start -->
                        <table class="table">
                            <thead>
                                <tr>                                    
                                    <th>Emergency</th>
                                    <th>Raised By</th>
                                    <th>Location</th>
                                    <th>Status</th>
                                    <th>Created Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody class="emergency-listing">
                            </tbody>
                        </table>
                        <!-- Listing end -->
                    </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-default">
                            <a href="#" class="load-more-emergency" style="text-decoration: none;">Load More</a>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->

<!-- START DELETE EMERGENCY MODAL-->
<div id="cnf_delete_emergency" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this emergency?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" name="btn_del_cnf" id="btn_del_emergency_cnf">Confirm</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>
</div>
<!-- END DELETE EMERGENCY MODAL-->
<script>
    $(function () {
        var offset = 0;        
        getEmergencyListing('','',offset);
    });
</script>