<?php
$userdetails = $this->session->userdata('logged_in');
$username = $userdetails['name'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Petitup - Admin</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="<?= base_url() ?>css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->

        <!-- START CUSTOM CSS -->
        <link href="<?= base_url() ?>css/custom_style.css" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" type="text/css" />
        <!-- START CUSTOM CSS -->
        
        <!-- START BOOTSTRAP DATETIME PICKER CSS -->
        <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
        <!-- END BOOTSTRAP DATETIME PICKER CSS -->
        
        <!-- START PLUGINS -->
        <script type="text/javascript" src="<?= base_url() ?>js/plugins/jquery/jquery.min.js"></script>
        <!-- START PLUGINS -->
    </head>
    <body data-base_url="<?= base_url() ?>">
        <div class="error_msg"></div>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">

            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="Dashboard">Petiitup Admin</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?= base_url() ?>assets/images/users/avatar.jpg" alt="<?= $username ?>"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?= base_url() ?>assets/images/users/avatar.jpg" alt="<?= $username ?>"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?= $username ?></div>
                                <div class="profile-data-title">Admin</div>
                            </div>
                            <!--<div class="profile-controls">
                                <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>-->
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    <!-- <li class="<?php echo (strpos($_SERVER['PHP_SELF'], 'Dashboard') ? ' active' : ''); ?>">
                        <a href="<?php echo base_url() ?>Dashboard"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
                    </li> -->                    
                    <li class="<?php echo (strpos($_SERVER['PHP_SELF'], 'Adoption') ? ' active' : ''); ?>"><!--xn-openable-->
                        <a href="<?php echo base_url() ?>Adoption/"><span class="fa fa-files-o"></span> <span class="xn-text">Adoption</span></a>
                    </li>
                    <li class="<?php echo (strpos($_SERVER['PHP_SELF'], 'DateMate') ? ' active' : ''); ?>"><!--xn-openable-->
                        <a href="<?php echo base_url() ?>DateMate/"><span class="fa fa-files-o"></span> <span class="xn-text">Date and Mate</span></a>
                    </li>
                    <li class="<?php echo (strpos($_SERVER['PHP_SELF'], 'Tips') ? ' active' : ''); ?>">
                        <a href="<?php echo base_url() ?>Tips"><span class="fa fa-files-o"></span><span class="xn-text">Tips</span></a>
                    </li>
                    <li class="<?php echo (strpos($_SERVER['PHP_SELF'], 'Events') ? ' active' : ''); ?>">
                        <a href="<?php echo base_url() ?>Events"><span class="fa fa-files-o"></span><span class="xn-text">Events</span></a>
                    </li>
                    <li class="<?php echo (strpos($_SERVER['PHP_SELF'], 'Emergencies') ? ' active' : ''); ?>">
                        <a href="<?php echo base_url() ?>Emergency/"><span class="fa fa-files-o"></span> <span class="xn-text">Emergencies</span></a>
                    </li>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->