<!-- PAGE CONTENT -->
<div class="page-content" style="min-height: 662px;">

    <!-- START X-NAVIGATION VERTICAL -->
    <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
        <!-- TOGGLE NAVIGATION -->
        <li class="xn-icon-button">
            <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
        </li>
        <!-- END TOGGLE NAVIGATION -->
        <!-- SEARCH -->
        <li class="xn-search">
            <form role="form">
                <input type="text" name="search" placeholder="Search..."/>
            </form>
        </li>   
        <!-- END SEARCH -->
        <!-- SIGN OUT -->
        <li class="xn-icon-button pull-right">
            <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
        </li>
        <!-- END SIGN OUT -->
    </ul>
    <!-- END X-NAVIGATION VERTICAL -->                     

    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="<?= base_url() ?>Dashboard">Dashboard</a></li>                    
        <li><a href="<?= base_url() ?>Tips">Tips</a></li>
        <li class="active">Tips Answers</li>
    </ul>
    <!-- END BREADCRUMB -->                

    <div class="page-title">                    
        <h2><span class="fa fa-arrow-circle-o-left"></span> Tips Answers</h2>
    </div>                   

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">

        <div class="row">
            <div class="col-md-12">
                <!-- START SIMPLE DATATABLE -->
                <div class="panel panel-default tips-answer-panel">
                    <div class="panel-heading">                                
                        <h3 class="panel-title">Tips Answers Details</h3>
                        <a href="#" class="pull-right export-tips-answer-exl btn btn-default">Export to Excel</a>
                        <!--<ul class="panel-controls" style="margin-top: 2px;">
                            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>                                       
                        </ul>-->
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="input-group">
                                <input type="text" class="form-control tips-answer-search-box" placeholder="Search by answer and user name" maxlength="50">
                                <div class="input-group-btn">
                                    <div class="btn-group" role="group">
                                        <div class="dropdown dropdown-lg">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><span class="caret"></span></button>
                                            <button type="button" class="btn btn-default clear-tips-answer-filter hide" title="clear filter"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                                            <div class="dropdown-menu dropdown-menu-right" role="menu" style="min-width: 543px;padding: 15px;">
                                                <div class="row">
                                                    <form name="frm_srch_tips_ans" id="frm_srch_tips_ans" role="form" method="POST">
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <label class="label-control">From Date</label>
                                                                <div class="input-group from-date-picker from-date">
                                                                    <input class="form-control" type="text" name="from_tips_ans_date" id="from_tips_ans_date" placeholder="">
                                                                    <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <label class="label-control">To Date</label>
                                                                <div class="input-group to-date-picker from-date">
                                                                    <input class="form-control" type="text" name="to_tips_ans_date" id="to_tips_ans_date" placeholder="">
                                                                    <span class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12" style="margin-top: 10px;">
                                                                <button type="submit" name="btn-tips-answer-filter" id="btn-tips-answer-filter" class="btn btn-default pull-right">
                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table tabletipsansexcel">
                            <thead>
                                <tr>
                                    <th>Answer</th>
                                    <th>User</th>
                                    <th>Up votes</th>
                                    <th>Down votes</th>
                                    <th>Spam</th>
                                    <th>Created Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="tips-answer-listing">
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-default">
                            <a href="#" class="load-more-tips-answer" style="text-decoration: none;">Load More</a>
                        </button>
                    </div>
                </div>
                <!-- END SIMPLE DATATABLE -->

            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT WRAPPER -->                
</div>            
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<!-- START DELETE QUESTION MODAL-->
<div id="cnf_delete_ans" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this answer?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" name="btn_del_ans_cnf" id="btn_del_ans_cnf">Confirm</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
            </div>
        </div>

    </div>
</div>
<!-- END DELETE QUESTION MODAL-->

<script>
    $(function () {
        var id = urlid[urlid.length - 1];
        var extra = {"search_text": "", "from_date": "", "to_date": ""}
        getTipsAnswers(id, extra);
    });
</script>