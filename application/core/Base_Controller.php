<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Base_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();

        $session = $this->session->userdata('logged_in');
        if (empty($session)) {
            redirect('Login');
        }
    }

    function render($page, $data) {
        $this->load->view('templates/header');
        $this->load->view($page, $data);
        $this->load->view('templates/footer');
    }

}
