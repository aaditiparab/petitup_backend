<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Events_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Function - getTipsDetails
     * 
     * 
     */
    public function get_Events_Details($offset = 0) {
        $this->db->select("pe.e_id, pe.e_user_id,pe.e_name, pe.e_category,pe.e_description,pe.e_location,pe.city,pe.total_attendance,pe.lat,pe.lng,"
                . "pe.e_start_date, pe.e_end_date,pe.e_created_date, CONCAT(pup.FirstName,' ',pup.LastName) as user_name,pup.profileImg");
        $this->db->from('piu_events pe');
        $this->db->join('piu_user_profile pup', 'pup.id = pe.e_user_id', 'INNER');
        $this->db->where('pe.e_isdeleted = 0');
        $this->db->order_by('pe.e_id DESC');
        $this->db->limit(11);
        $this->db->offset($offset);
        $resultset = $this->db->get()->result_array();

        if (count($resultset) > 10) {
            $data['is_availeble'] = '1';
            unset($resultset[count($resultset) - 1]); //removing last element from array of result set
        } else {
            $data['is_availeble'] = '0';
        }

        $data['resultset'] = $resultset;
        return $data;
    }

    /**
     * createUpdateEvent
     * 
     * @param string $question this is question 
     */
    function createUpdateEvent() {
        $eventid = $this->input->post('eventid');
        $eventname = $this->input->post('eventname');
        $eventdesc = $this->input->post('eventdesc');
        $eventlocation = $this->input->post('eventlocation');
        $eventcity = $this->input->post('eventcity');
        $eventlat = $this->input->post('eventlat');
        $eventlng = $this->input->post('eventlng');
        $startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');

        if ($eventid == "0") {
            $insert = array(
                "e_name" => $eventname,
                "e_user_id" => "15",
                "e_description" => $eventdesc,
                "e_location" => $eventlocation,
                "city" => $eventcity,
                "lat" => $eventlat,
                "lng" => $eventlng,
                "e_start_date" => $startdate,
                "e_end_date" => $enddate,
                "e_created_date" => date("Y-m-d H:i:s")
            );
            $this->db->insert('piu_quora_question', $insert);
            return $result['message'] = "Tip created successfully.";
        } else {

            $update = array(
                "qr_question" => $question
            );
            $this->db->where(array('qr_id' => $quesid, "qr_userid" => "15"));
            $this->db->update('piu_quora_question', $update);
            return $result['message'] = "Tip updated successfully.";
        }
    }

    function delete_Events($eventid) {
        $update = array(
            "e_isdeleted" => "1"
        );
        $this->db->where("e_id = '$eventid'");
        $result = $this->db->update('piu_events', $update);

        if ($result) {
            $data = array("status" => TRUE, "message" => "Event deleted successfully.");
        } else {
            $data = array("status" => FALSE, "message" => "Something went wrong, please try again later.");
        }

        return $data;
    }

}
