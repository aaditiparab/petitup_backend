<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Tips_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Function - getTipsDetails
     * 
     * 
     */
    public function getTipsDetails($offset = 0, $extra) {

        $search_text = $extra['search_text'];
        $from_date = $extra['from_date'];
        $to_date = $extra['to_date'];

        $this->db->select("pqq.qr_id, pqq.qr_question,pqq.qr_userid, pqq.total_spamed,pqq.qr_total_answers,pqq.qr_createddate, "
                . "CONCAT(pup.FirstName,' ',pup.LastName) as ques_by_name,pup.profileImg");
        $this->db->from('piu_quora_question pqq');
        $this->db->join('piu_user_profile pup', 'pup.id = pqq.qr_userid', 'INNER');
        $this->db->where('pqq.qr_isdeleted = 0');

        if ($search_text != "") {
            $this->db->where("pqq.qr_question like '%$search_text%' OR pup.FirstName like '%$search_text%'");
        }

        if ($from_date != "" && $to_date != "") {
            $this->db->where("Date_Format(pqq.qr_createddate, '%d-%m-%Y') >= '$from_date' AND Date_Format(pqq.qr_createddate, '%d-%m-%Y') <= '$to_date'");
        }

        $this->db->order_by('pqq.qr_id DESC');
        $this->db->limit(11);
        $this->db->offset($offset);
        $resultset = $this->db->get()->result_array();

        if (count($resultset) > 10) {
            $data['is_availeble'] = '1';
            unset($resultset[count($resultset) - 1]); //removing last element from array of result set
        } else {
            $data['is_availeble'] = '0';
        }

        $data['resultset'] = $resultset;
        return $data;
    }

    /**
     * createUpdateTips
     * 
     * @param string $question this is question 
     */
    function createUpdateTips($quesid, $question) {
        if ($quesid == "0") {
            $insert = array(
                "qr_question" => $question,
                "qr_userid" => "15",
                "qr_createddate" => date("Y-m-d H:i:s")
            );
            $this->db->insert('piu_quora_question', $insert);
            return $result['message'] = "Tip created successfully.";
        } else {

            $update = array(
                "qr_question" => $question
            );
            $this->db->where(array('qr_id' => $quesid, "qr_userid" => "15"));
            $this->db->update('piu_quora_question', $update);
            return $result['message'] = "Tip updated successfully.";
        }
    }

    /**
     * Function - getTipsAnswers
     * @param int $quesid This is question id on which basis answers need to be fetch
     * @param int $offset this will be used for fetching more records load more
     * 
     */
    public function getTipsAnswers($quesid, $offset = 0, $extra) {

        $search_text = $extra['search_text'];
        $from_date = $extra['from_date'];
        $to_date = $extra['to_date'];

        $this->db->select("pqa.qr_id as answerid,pqa.qr_q_id as quesid,pqa.qr_answer as answer,pqa.qr_userid as userid,pqa.qr_createddate as createddate,"
                . "CONCAT(pup.FirstName,' ',pup.LastName) as name,pqa.total_upvote, pqa.total_downvote,pqa.total_spamed, "
                . "COALESCE(trs.status,0) as is_spamed");
        $this->db->from('piu_quora_answers pqa');
        $this->db->join('piu_user_profile pup', 'pup.id = pqa.qr_userid', 'INNER');
        $this->db->join('tips_report_spam trs', 'trs.question_id = pqa.qr_q_id AND trs.user_id = qr_userid', 'LEFT');
        $this->db->where(array("pqa.qr_q_id" => $quesid, "pqa.qr_isdeleted" => "0"));

        if ($search_text != "") {
            $this->db->where("pqa.qr_answer like '%$search_text%' OR pup.FirstName like '%$search_text%'");
        }

        if ($from_date != "" && $to_date != "") {
            $this->db->where("Date_Format(pqa.qr_createddate, '%d-%m-%Y') >= '$from_date' AND Date_Format(pqa.qr_createddate, '%d-%m-%Y') <= '$to_date'");
        }

        $this->db->limit(11);
        $this->db->offset($offset);
        $this->db->order_by('pqa.total_upvote DESC'); //highest voted ans comes first
        $resultset = $this->db->get()->result_array();

        if (count($resultset) > 10) {
            $data['is_availeble'] = '1';
            unset($resultset[count($resultset) - 1]); //removing last element from array of result set
        } else {
            $data['is_availeble'] = '0';
        }

        $data['resultset'] = $resultset;
        return $data;
    }

    function delete_tips($quesid) {
        $update = array(
            "qr_isdeleted" => "1"
        );
        $this->db->where("qr_id = '$quesid'");
        $result = $this->db->update('piu_quora_question', $update);

        if ($result) {
            $data = array("status" => TRUE, "message" => "Question deleted successfully.");
        } else {
            $data = array("status" => FALSE, "message" => "Something went wrong, please try again later.");
        }

        return $data;
    }

    function delete_tips_answer($ansid) {
        $update = array(
            "qr_isdeleted" => "1"
        );
        $this->db->where("qr_id = '$ansid'");
        $result = $this->db->update('piu_quora_answers', $update);

        if ($result) {
            $data = array("status" => TRUE, "message" => "Answer deleted successfully.");
        } else {
            $data = array("status" => FALSE, "message" => "Something went wrong, please try again later.");
        }

        return $data;
    }

}
