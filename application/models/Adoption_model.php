<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Adoption_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getOwnerDetails($id, $offset = 0, $search, $gender, $fromDate, $toDate)
    {    
        
        if($id == '' || $id == 0)
        {     
            $this->db->select("DISTINCT(ownerid) as id ,name, (case gender when 1 then 'male' when 2 then 'female' else 'others' end) as gender, location, ifnull(description,'') as description , (select count(*) from ad_pets ap where ap.adp_ownerid = aup.ownerid) as pets, ifnull((select GROUP_CONCAT(m_media) from piu_media where m_postid = aup.ownerid and m_type = 6),'') as images, (select count(*) from ad_shortlist where ads_user_id = aup.ownerid) as interests,aup.createddate");
            $this->db->from("ad_user_profile aup");        
            $this->db->where("ifnull(aup.isdeleted,0)",0);
            if($search != '')
            {
                $this->db->where("(name like '%".$search."%' OR description like '%".$search."%' OR location like '%".$search."%')");
            }

            if($gender != '')
            {
                $this->db->where("(gender = $gender)");
            }

            if($fromDate != '' && $toDate != '')
            {
                $this->db->where("Date_Format(aup.createddate, '%Y-%m-%d') >= '$fromDate'");
                $this->db->where("Date_Format(aup.createddate, '%Y-%m-%d') <= '$toDate'");
            }

            $this->db->order_by('aup.createddate DESC');
            
        }
        else
        {
            $this->db->select("DISTINCT(ownerid) as id ,name, (case gender when 1 then 'male' when 2 then 'female' else 'others' end) as gender, location, ifnull(description,'') as description , (select count(*) from ad_pets ap where ap.adp_ownerid = aup.ownerid) as pets, ifnull((select GROUP_CONCAT(m_media) from piu_media where m_postid = aup.ownerid and m_type = 6),'') as images, (select count(*) from ad_shortlist where ads_user_id = aup.ownerid) as interests,aup.createddate");
            $this->db->from("ad_requests ar");
            $this->db->join("ad_pets ap","ar.adr_pet = ap.adp_id");
            $this->db->join("ad_user_profile aup","ar.adr_from = aup.ownerid");
            $this->db->where("ap.adp_id",$id);
            if($search != '')
            {
                $this->db->where("(name like '%".$search."%' OR description like '%".$search."%' OR location like '%".$search."%')");
            }

            if($gender != '')
            {
                $this->db->where("(gender = $gender)");
            }

            if($fromDate != '' && $toDate != '')
            {
                $this->db->where("Date_Format(aup.createddate, '%Y-%m-%d') >= '$fromDate'");
                $this->db->where("Date_Format(aup.createddate, '%Y-%m-%d') <= '$toDate'");
            }

            $this->db->order_by('aup.createddate DESC');

        }

        $this->db->limit(11);
        $this->db->offset($offset);
        $resultset = $this->db->get()->result_array();

        //echo $this->db->last_query();exit;
        
        if (count($resultset) > 10) {
            $data['is_available'] = '1';
            unset($resultset[count($resultset) - 1]); //removing last element from array of result set
        } else {
            $data['is_available'] = '0';
        }

        $data['resultset'] = $resultset;
        return $data;
    }

    function deleteAdoptionOwner($id)
    {
        $update = array(
            "isdeleted" => "1",
            "deleteddate"=> now()
        );
        $this->db->where("id", $id);
        $result = $this->db->update('ad_user_profile', $update);

        if ($result) {
            $data = array("status" => TRUE, "message" => "Adoption owner deleted successfully.");
        } else {
            $data = array("status" => FALSE, "message" => "Something went wrong, please try again later.");
        }

        return $data;
    }

    function getOwnerPetDetails($id, $offset, $search, $gender, $fromDate, $toDate)
    {
        $this->db->select("ap.adp_id as id, ifnull(ap.adp_name,'-') as petname,ifnull(ap.adp_description,'') as description, DATE_FORMAT(ap.adp_dob, '%d/%m/%Y') as dob,pa.pa_name as category, ifnull(pb.pb_name,'-') as breed, (case ifnull(ap.adp_purebreed,0) when 0 then 'No' else 'Yes' end) as is_pure_breed, ifnull(ap.adp_description,'-') as description, (case ap.adp_gender when 1 then 'male' when 2 then 'female' else 'others' END) as gender, (case ifnull(ap.is_adopted,0) when 0 then 'No' else 'Yes' end) as is_adopted, (case ifnull(ap.is_for_adoption,0) when 0 then 'No' else 'Yes' end) as is_for_adoption, ap.adp_created as createddate, ifnull((select count(*) from ad_requests ar where ar.adr_pet = ap.adp_id),0) as requests, ap.adp_ownerid as ownerid");
        $this->db->from("ad_pets ap");
        $this->db->join("piu_animal pa", "ap.adp_animal = pa.pa_id", "LEFT");
        $this->db->join("piu_breeds pb", "pb.pb_id = ap.adp_breed", "LEFT");
        $this->db->where(array("ifnull(ap.adp_isdeleted,0)"=> 0,"ap.adp_ownerid"=> $id));

        if($search != '')
        {
            $this->db->where("(adp_name like '%".$search."%' or adp_description like '%".$search."%')");
        }

        if($gender != '')
        {
            $this->db->where("(adp_gender = $gender)");
        }

        if($fromDate != '' && $toDate != '')
        {
            $this->db->where("Date_Format(ap.adp_created, '%Y-%m-%d') >= '$fromDate'");
            $this->db->where("Date_Format(ap.adp_created, '%Y-%m-%d') <= '$toDate'");
        }

        $this->db->order_by('ap.adp_created DESC');
        $this->db->limit(11);
        $this->db->offset($offset);
        
        $resultset = $this->db->get()->result_array();
        if (count($resultset) > 10) {
            $data['is_available'] = '1';
            unset($resultset[count($resultset) - 1]); //removing last element from array of result set
        } else {
            $data['is_available'] = '0';
        }

        $data['resultset'] = $resultset;
        return $data;
    }

    function deleteAdoptionPet($id)
    {
        $update = array(
            "adp_isdeleted" => "1",
            "adp_deleteddate"=> now()
        );
        $this->db->where("adp_id", $id);
        $result = $this->db->update('ad_pets', $update);

        if ($result) {
            $data = array("status" => TRUE, "message" => "Adoption pet deleted successfully.");
        } else {
            $data = array("status" => FALSE, "message" => "Something went wrong, please try again later.");
        }

        return $data;
    }

    function getFavoritePets($id)
    {
        $this->db->select("aup.name as ownername, ap.adp_name as petname, ap.adp_id as petid,ifnull((select m_media from piu_media where m_postid = aup.ownerid and m_type = 6 and media_type = 1 limit 1),'') as userimg, ifnull((select m_media from piu_media where m_postid = ap.adp_id and m_type = 2 and media_type = 1 limit 1),'') as petimg");
        $this->db->from("ad_user_profile aup");
        $this->db->join("ad_pets ap","aup.ownerid = ap.adp_ownerid");
        $this->db->join("ad_shortlist asl","asl.ads_pet = ap.adp_id");
        $this->db->where("asl.ads_user_id", $id);

        $resultset = $this->db->get()->result_array();
        $data['resultset'] = $resultset;
        return $data;
    }    
    
}