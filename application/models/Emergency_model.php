<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Emergency_model extends CI_Model {

	function __construct() {
        parent::__construct();
    }

    function getEmergencyListing($offset = 0, $search, $status, $fromDate, $toDate)
    {    
        $this->db->select("e.em_id as id,e.em_user_id AS userid, CONCAT( u.FirstName,  ' ', u.LastName ) AS name, e.em_description AS description, e.em_location AS location, ifnull(e.em_city,'') as city,e.em_latitude as latitude, e.em_longitude as longitude,e.em_createddate as createddate, u.profileimg as profileimg,
			 ifnull(e.em_status,0) as status");
		$this->db->from("piu_emergency e");
		$this->db->join("piu_user_profile u", "e.em_user_id = u.id");
		$this->db->where("e.em_isdeleted",0);
        if($search != '')
        {
            $this->db->where("(u.FirstName like '%".$search."%' OR u.LastName like '%".$search."%' OR e.em_description like '%".$search."%' OR e.em_location like '%".$search."%')");
        }

        if($status != '')
        {
            $this->db->where("(e.em_status = $status)");
        }

        if($fromDate != '' && $toDate != '')
        {
            $this->db->where("Date_Format(e.em_createddate, '%Y-%m-%d') >= '$fromDate'");
            $this->db->where("Date_Format(e.em_createddate, '%Y-%m-%d') <= '$toDate'");
        }

        $this->db->limit(11);
        $this->db->offset($offset);        
        $resultset = $this->db->get()->result_array();
        //print_r( $this->db->last_query());exit;
        
        if (count($resultset) > 10) {
            $data['is_available'] = '1';
            unset($resultset[count($resultset) - 1]); //removing last element from array of result set
        } else {
            $data['is_available'] = '0';
        }

        $data['resultset'] = $resultset;
        return $data;
    }

    function deleteEmergency($id)
    {   
        $update_arr = array(
            "em_isdeleted" => "1"
            );
        $this->db->where(array("em_id"=> $id));
        $result =  $this->db->update("piu_emergency", $update_arr);


        if ($result) {
            $data = array("status" => TRUE, "message" => "Emergency deleted successfully.");
        } else {
            $data = array("status" => FALSE, "message" => "Something went wrong, please try again later.");
        }

        return $data;
    }

    function updateEmergencyStatus($id, $status)
    {
        $update_arr = array(
            "em_status"=> $status
            );
        $this->db->where(array("em_id"=> $id));
        $result = $this->db->update("piu_emergency", $update_arr);

        if ($result) {
            $data = array("status" => TRUE, "message" => "Emergency status updated successfully.");
        } else {
            $data = array("status" => FALSE, "message" => "Something went wrong, please try again later.");
        }

        return $data;
    }
}