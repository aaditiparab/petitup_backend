var base_url = $('body').attr('data-base_url');
var urlid = window.location.href.split("/");
var defaultimg = base_url + 'img/defaultuser.png';

/* START show_loader
 *  Common function for loading symbol till data appears */
function show_loader(panel) {
    if (!panel.hasClass('panel-data-loading')) {
        panel.addClass('panel-data-loading');
        panel.append('<div class="panel-refresh-layer"><img src="' + base_url + 'img/loaders/default.gif"/></div>');
        panel.find(".panel-refresh-layer").width(panel.width()).height(panel.height());
    } else {
        panel.removeClass('panel-data-loading');
        panel.find('.panel-refresh-layer').remove();
    }
}
/* END show_loader */

/* START show_error
 *  Common function for show success or errors messages as toast, appears at bottom right */
show_error = function (message, status) {
    var error_type = (status) ? "btn-success" : "btn-danger";
    $(".error_msg").html('<div class="btn ' + error_type + '">' + message + '</div>').fadeIn(2000).delay(2000).fadeOut(2000);
}
/* END show_error */

/* Convert Date */

function convertDate(input) {
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var d = new Date(input);

    function pad(s) {
        return (s < 10) ? '0' + s : s;
    }

    var hours = d.getHours();
    var ampm = hours >= 12 ? 'PM' : 'AM';

    var date = [pad(d.getDate()), months[d.getMonth()], d.getFullYear()].join('-');
    var time = [pad(hours), pad(d.getMinutes()), pad(d.getSeconds())].join(':');

    return date + ' ' + time + ' ' + ampm;
}

/* Convert Date */

/* START Common ajax call */
function ajax_call(url, method, param, cb) {
    $.ajax({
        url: url,
        method: method,
        data: param
    }).done(function (result) {
        cb(result);
    });
}
/* END Common ajax call */

$(function () {

    var offset = 0;
    var extra = {"search_text": "", "from_date": "", "to_date": ""}
    /* 
     * Tips Module START 
     */

    /* Tips Question Load START */
    $(".load-more-tips").on("click", function () {
        offset = offset + 10;

        var panel = $(this).parents(".panel");
        show_loader(panel);

        getTipsDetails(offset, extra, function () {
            setTimeout(function () {
                show_loader(panel);
            }, 500);
        });
        return false;
    });
    /* Tips Question Load END */

    /* Tips Answer Load START */
    $(".load-more-tips-answer").on("click", function () {
        offset = offset + 10;

        var panel = $(this).parents(".panel");
        show_loader(panel);

        var id = urlid[urlid.length - 1];

        getTipsAnswers(id, extra, offset, function () {
            setTimeout(function () {
                show_loader(panel);
            }, 500);
        });
        return false;
    });
    /* Tips Answer Load END */

    /* 
     * Tips Module END
     */

    /*
     * Event Module START
     */

    /* Events Load START */
    $(".load-more-events").on("click", function () {
        offset = offset + 10;

        var panel = $(this).parents(".panel");
        show_loader(panel);

        getEventsDetails(offset, function () {
            setTimeout(function () {
                show_loader(panel);
            }, 500);
        });
        return false;
    });
    /* Events Load END */

    /*
     * Event Module END
     */

    /*
     * Emergency Module START
     */

    $(".load-more-emergency").on("click", function () {
        offset = offset + 10;

        var panel = $(this).parents(".panel");
        show_loader(panel);

        getEmergencyListing(offset, function () {
            setTimeout(function () {
                show_loader(panel);
            }, 500);
        });
        return false;
    });

    /*
     * Emergency Module START
     */

    /* START Tips filter */

    //Search by question and users name
    $('.tips-search-box').on('keyup', function (e) {
        extra = {"search_text": $(this).val(), "from_date": "", "to_date": ""}
        getTipsDetails(offset, extra);
    });

    //Apply date filter on Tips
    $('#frm_srch_tips').on('submit', function (e) {
        e.preventDefault();
        var from_date = $("#from_date").val();
        var to_date = $("#to_date").val();

        extra = {"search_text": "", "from_date": from_date, "to_date": to_date}
        getTipsDetails(offset, extra);
        $(this).closest('.dropdown').removeClass('open');
        $(this).closest('.dropdown').find('button.clear-tips-filter').removeClass('hide');
    });

    //Clear Tips date filter
    $('button.clear-tips-filter').on('click', function () {
        window.location.reload();
    });

    /* END Tips filter */

    /* START Tips ANSWER FILTER */

    //Search by answer and users name
    $(".tips-answer-search-box").on('keyup', function () {
        extra = {"search_text": $(this).val(), "from_date": "", "to_date": ""}
        var id = urlid[urlid.length - 1];
        getTipsAnswers(id, extra, offset);
    });

    //Apply date filter on Tips Answer
    $('#frm_srch_tips_ans').on('submit', function (e) {
        e.preventDefault();
        var from_date = $("#from_tips_ans_date").val();
        var to_date = $("#to_tips_ans_date").val();

        extra = {"search_text": "", "from_date": from_date, "to_date": to_date}
        var id = urlid[urlid.length - 1];
        getTipsAnswers(id, extra, offset);
        $(this).closest('.dropdown').removeClass('open');
        $(this).closest('.dropdown').find('button.clear-tips-answer-filter').removeClass('hide');
    });

    //Clear Tips answer date filter
    $('button.clear-tips-answer-filter').on('click', function () {
        window.location.reload();
    });

    /* END Tips ANSWER FILTER */
});

/* START Load Tips */
function getTipsDetails(offset, extra, cb) {
    var param = {offset: offset, extra: extra}
    var tips_html = '';

    ajax_call(base_url + 'Tips/getTipsDetails', 'POST', param, function (result) {
        result = JSON.parse(result);

        if (result['is_availeble'] == '0') {
            $(".load-more-tips").parents('.panel-footer').hide();
        }
        else {
            $(".load-more-tips").parents('.panel-footer').show();
        }

        if (result['resultset'].length == 0) {
            tips_html = '<tr><td colspan="6" class="text-center">No records found</td></tr>';
            $('.panel.tips-panel .table tbody.tips-listing').html(tips_html);
        }

        for (var i in result['resultset']) {
            tips_html += '<tr class="tips_' + result['resultset'][i].qr_id + '">\
                        <td>' + result['resultset'][i].qr_question + '</td>\
                        <td>' + result['resultset'][i].ques_by_name + '</td>\
                        <td>' + result['resultset'][i].total_spamed + '</td>\
                        <td class="text-center"><a href="' + base_url + 'Tips/tipsAnswers/' + result['resultset'][i].qr_id + '" class="" target="_blank">' + result['resultset'][i].qr_total_answers + '</a></td>\
                        <td class="text-center">' + convertDate(result['resultset'][i].qr_createddate) + '</td>\
                        <td class="text-center">\
                            <a href="#" data-toggle="modal" data-target="#add_update_ques" data-quesid="' + result['resultset'][i].qr_id + '" data-question="' + result['resultset'][i].qr_question + '" class="glyphicon glyphicon-pencil pencil-icon"></a>&nbsp;&nbsp;\
                            <a href="#" data-toggle="modal" data-target="#cnf_delete_ques" data-quesid="' + result['resultset'][i].qr_id + '" class="glyphicon glyphicon-trash trash-icon"></a>\
                        </td>\
                    </tr>';
        }

        if (offset == 0) {
            $('.panel.tips-panel table tbody.tips-listing').html(tips_html);
        }
        else {
            $('.panel.tips-panel table tbody.tips-listing').append(tips_html);
        }

        if (offset > 0) {
            cb(true);
        }
    });
}
/* END Load Tips */

$("#add_update_ques").on('show.bs.modal', function (e) {
    var _this = $(e.relatedTarget);
    var quesid = _this.attr('data-quesid');
    var question = _this.attr('data-question');

    $("#add_update_ques #updatequesid").val(quesid);
    $("#add_update_ques #tip_detail").val(question);
});

/* START Create New Tip */

$("#add_update_ques button#btn_create_tip").on('click', function () {
    var base = "#add_update_ques ";
    var quesid = $(base + "#updatequesid").val();
    var question = $(base + "#tip_detail").val();

    var create_ques_param = {quesid: quesid, question: question}
    ajax_call(base_url + 'Tips/create_update_tips', 'POST', create_ques_param, function (result) {

        result = JSON.parse(result);
        if (result.status) {
            $("#add_update_ques [data-dismiss=modal]").trigger("click");
            show_error(result.message, true);
            setTimeout(function () {
                window.location.reload();
            }, 2000);
        } else {
            show_error(result.message, false);
        }
    });
});

/* START Create New Tip */

/* START Delete QUESTION modal box show */
$("#cnf_delete_ques").on('show.bs.modal', function (event) {
    var quesid = $(event.relatedTarget).attr('data-quesid');
    $(this).find('#btn_del_cnf').attr('data-quesid', quesid);
});
/* END Delete QUESTION moodal box show */

/* START Confirm delete Tips */
$("#cnf_delete_ques #btn_del_cnf").on('click', function (e) {
    var id = $(this).attr('data-quesid');
    var del_ques_param = {quesid: id}
    ajax_call(base_url + 'Tips/deleteTips', 'POST', del_ques_param, function (result) {
        result = JSON.parse(result)

        if (result.status) {
            show_error(result.message, true);
            $("tr.tips_" + id).addClass('animated bounceOutLeft').slideUp(500);
            //$("tr.tips_" + id).slideUp("slow", function() { $("tr.tips_" + id).remove();});
            $('#cnf_delete_ques [data-dismiss="modal"]').trigger('click');
        } else {
            show_error(result.message, false);
        }
    });
});
/* END Confirm delete Tips */

//Date picker initialize on tips details
$(".from-date-picker,.to-date-picker").datetimepicker({
    useCurrent: false,
    format: 'DD-MM-YYYY'
});

/* START Export tips detail into excel */
$(".export-tips-exl").on('click', function (e) {
    e.preventDefault();
    $(".tabletipsexcel").table2excel({
        exclude: ".noExl",
        exclude_img: true,
        exclude_links: true,
        exclude_inputs: true,
        name: "Excel Document Name",
        filename: "Tips Details",
        fileext: ".xls"
    });
});
/* END Export tips detail into excel */

/* Load Tips Answers */
function getTipsAnswers(id, extra, offset, cb) {
    var param = {quesid: id, offset: offset, extra: extra}
    var tips_html = '';

    ajax_call(base_url + 'Tips/getTipsAnswers/', 'POST', param, function (result) {
        result = JSON.parse(result);

        if (result['is_availeble'] == '0') {
            $(".load-more-tips-answer").parents('.panel-footer').hide();
        }
        else {
            $(".load-more-tips-answer").parents('.panel-footer').show();
        }

        if (result['resultset'].length == 0) {
            tips_html = '<tr><td colspan="6" class="text-center">No records found</td></tr>';
            //$('.panel.tips-answer-panel .panel-body table').hide();
            $('.panel.tips-answer-panel table tbody.tips-answer-listing').html(tips_html);
        }

        for (var i in result['resultset']) {

            tips_html += '<tr class="tips_ans_' + result['resultset'][i].answerid + '">\
                        <td>' + result['resultset'][i].answer + '</td>\
                        <td>' + result['resultset'][i].name + '</td>\
                        <td>' + result['resultset'][i].total_upvote + '</td>\
                        <td>' + result['resultset'][i].total_downvote + '</td>\
                        <td>' + result['resultset'][i].total_spamed + '</td>\
                        <td>' + convertDate(result['resultset'][i].createddate) + '</td>\
                        <td><a href="#" data-toggle="modal" data-target="#cnf_delete_ans" data-ansid="' + result['resultset'][i].answerid + '" class="glyphicon glyphicon-trash trash-icon"></a>\</td>\
                    </tr>';
        }

        if (offset == 0) {
            $('.panel.tips-answer-panel table tbody.tips-answer-listing').html(tips_html);
        }
        else {
            $('.panel.tips-answer-panel table tbody.tips-answer-listing').append(tips_html);
        }

        if (offset > 0) {
            cb(true);
        }
    });
}

/* START Delete Answer modal box show */
$("#cnf_delete_ans").on('show.bs.modal', function (event) {
    var ansid = $(event.relatedTarget).attr('data-ansid');
    $(this).find('#btn_del_ans_cnf').attr('data-ansid', ansid);
});
/* END Delete Answer modal box show */

/* START Confirm delete Tips Answer */
$("#cnf_delete_ans #btn_del_ans_cnf").on('click', function (e) {
    var id = $(this).attr('data-ansid');
    var del_ans_param = {ansid: id}
    ajax_call(base_url + 'Tips/deleteTipsAnswer', 'POST', del_ans_param, function (result) {
        result = JSON.parse(result)

        if (result.status) {
            show_error(result.message, true);
            $("tr.tips_ans_" + id).addClass('animated bounceOutLeft').slideUp(500);
            $('#cnf_delete_ans [data-dismiss="modal"]').trigger('click');
        } else {
            show_error(result.message, false);
        }
    });
});
/* END Confirm delete Tips Answer */

/* START Export tips answers detail into excel */
$(".export-tips-answer-exl").on('click', function (e) {
    e.preventDefault();
    $(".tabletipsansexcel").table2excel({
        exclude: ".noExl",
        exclude_img: true,
        exclude_links: true,
        exclude_inputs: true,
        name: "Excel Document Name",
        filename: "Tips Answer Details",
        fileext: ".xls"
    });
});
/* END Export tips answers detail into excel */

/* START Load Events */
function getEventsDetails(offset, cb) {
    var param = {offset: offset}
    var event_html = '';

    ajax_call(base_url + 'Events/getEventsDetails', 'POST', param, function (result) {
        result = JSON.parse(result);

        if (result['is_availeble'] == '0') {
            $(".load-more-events").parents('.panel-footer').hide();
        }

        if (result['resultset'].length == 0) {
            event_html = '<div class="text-center">No records found</div>';
            $('.panel.tips-panel .panel-body table').hide();
            $('.panel.tips-panel .panel-body').html(event_html);
        }

        for (var i in result['resultset']) {
            event_html += '<tr class="events_' + result['resultset'][i].e_id + '">\
                        <td>' + result['resultset'][i].e_name + '</td>\
                        <td>' + result['resultset'][i].user_name + '</td>\
                        <td>' + result['resultset'][i].e_description + '</td>\
                        <td>' + result['resultset'][i].e_location + '</td>\
                        <td>' + result['resultset'][i].city + '</td>\
                        <td>' + result['resultset'][i].total_attendance + '</td>\
                        <td class="text-center">' + convertDate(result['resultset'][i].e_start_date) + '</td>\
                        <td class="text-center">' + convertDate(result['resultset'][i].e_end_date) + '</td>\
                        <td class="text-center">' + convertDate(result['resultset'][i].e_created_date) + '</td>\
                        <td class="text-center"><a href="#" data-toggle="modal" data-target="#cnf_delete_events" data-eventid="' + result['resultset'][i].e_id + '" class="btn btn-default">Delete</a></td>\
                    </tr>';
        }

        $('.panel.tips-panel table tbody.events-listing').append(event_html);

        if (offset > 0) {
            cb(true);
        }
    });
}
/* END Load Events */

/* START Event Delete modal box show */
$("#cnf_delete_events").on('show.bs.modal', function (event) {
    var eventid = $(event.relatedTarget).attr('data-eventid');
    $(this).find('#btn_del_evnt_cnf').attr('data-eventid', eventid);
});
/* END Event Delete moodal box show */

/* START Confirm delete Events */
$("#cnf_delete_events #btn_del_evnt_cnf").on('click', function (e) {
    var eventid = $(this).attr('data-eventid');
    var del_event_param = {eventid: eventid}
    ajax_call(base_url + 'Events/deleteEvents', 'POST', del_event_param, function (result) {
        result = JSON.parse(result)

        if (result.status) {
            show_error(result.message, true);
            $("tr.events_" + eventid).addClass('animated bounceOutLeft').slideUp(500);
            $('#cnf_delete_events [data-dismiss="modal"]').trigger('click');
        } else {
            show_error(result.message, false);
        }
    });
});
/* END Confirm delete Events */


/*   Aaditi  */

// Load Category DropDown //

$(document).ready(function(){

ajax_call(base_url + 'Pets/getCategory', 'POST', function (result) {
    result = JSON.parse(result);
    alert(result);
});

});

// End Category DropDown //

/* Load Adoption Owner Details */
function getAdoptionOwnerDetails(search, gender, fromDate, toDate, id, offset, cb) {

    var param = {search: search, gender: gender, fromDate: fromDate, toDate: toDate, id: id, offset: offset}
    var adp_owner_html = '';
    var img;

    ajax_call(base_url + 'Adoption/getOwnerDetails', 'POST', param, function (result) {
        result = JSON.parse(result);

        if (result['is_available'] == '0') {
            $(".load-more-adoption").parents('.panel-footer').hide();
        }

        if (result['resultset'].length == 0) {

            adp_owner_html = '<tr><td colspan="8" class="text-center"><b>NO ADOPTION OWNERS FOUND</b></td></tr>';
            $('.panel.adopt-panel table tbody.adoption-listing').html(adp_owner_html);
        }

        $('.panel.adopt-panel table tbody.adoption-listing').empty();

        for (var i in result['resultset']) {

            adp_owner_html += '<tr class="adp_' + result['resultset'][i].id + '">\
                        <td>' + result['resultset'][i].name + '</td>\
                        <td>' + result['resultset'][i].gender + '</td>\
                        <td>' + result['resultset'][i].location + '</td>\
                        <td>' + result['resultset'][i].description + '</td>\
                        <td><a href="' + base_url + 'Adoption/adoptionpets/' + result['resultset'][i].id + '" class="" target="_blank">' + result['resultset'][i].pets + '</a></td>\
                        <td><a href="#" data-toggle="modal" data-target="#cnf_pets_interest" data-adp-owner-id="' + result['resultset'][i].id + '">' + result['resultset'][i].interests + '</a></td>\
                        <td>' + result['resultset'][i].createddate + '</td>\
                        <td><a href="#" data-toggle="modal" data-target="#cnf_delete_adp_owner" data-adp-owner-id="' + result['resultset'][i].id + '" class="glyphicon glyphicon-trash trash-icon"></a></td>\
                    </tr>';
        }

        $('.panel.adopt-panel table tbody.adoption-listing').append(adp_owner_html);

        if (offset > 0) {
            cb(true);
        }
    });
}
/* End Adoption Owner Details */

/* START Event Delete modal box show */
$("#cnf_delete_adp_owner").on('show.bs.modal', function (event) {
    var ownerid = $(event.relatedTarget).attr('data-adp-owner-id');
    $(this).find('#btn_del_adp_owner_cnf').attr('data-adp-owner-id', ownerid);
});
/* END Event Delete modal box show */

/* START Confirm Delete Adoption Owner */
$("#cnf_delete_adp_owner #btn_del_adp_owner_cnf").on('click', function (e) {
    var id = $(this).attr('data-adp-owner-id');
    var del_adp_param = {id: id}
    ajax_call(base_url + 'Adoption/deleteAdoptionOwner', 'POST', del_adp_param, function (result) {
        result = JSON.parse(result)

        if (result.status) {
            show_error(result.message, true);
            $("tr.adp_" + id).addClass('animated bounceOutLeft').slideUp(500);
            $('#cnf_delete_adp_owner [data-dismiss="modal"]').trigger('click');
        } else {
            show_error(result.message, false);
        }
    });
});
/* END Confirm Delete Adoption Owner */

/* START Adoption Pet Details */
function getAdoptionPetDetails(search, gender, fromDate, toDate, id, offset, cb) {
    var param = {search: search, gender: gender, fromDate: fromDate, toDate: toDate, id: id, offset: offset}
    var adp_pet_html = '';
    var img;

    ajax_call(base_url + 'Adoption/getPetDetails', 'POST', param, function (result) {
        result = JSON.parse(result);

        if (result['is_available'] == '0') {
            $(".load-more-adoption").parents('.panel-footer').hide();
        }

        if (result['resultset'].length == 0) {
            adp_pet_html = '<tr><td colspan="12" class="text-center"><b>NO ADOPTION PETS FOUND</b></td></tr>';
            $('.panel.adopt-panel table tbody.adoption-listing').html(adp_pet_html);
        }

        $('.panel.adopt-panel table tbody.adoption-listing').empty();

        for (var i in result['resultset']) {

            adp_pet_html += '<tr class="adp_' + result['resultset'][i].id + '">\
                        <td>' + result['resultset'][i].petname + '</td>\
                        <td>' + result['resultset'][i].description + '</td>\
                        <td>' + result['resultset'][i].gender + '</td>\
                        <td>' + result['resultset'][i].dob + '</td>\
                        <td>' + result['resultset'][i].category + '</td>\
                        <td>' + result['resultset'][i].breed + '</td>\
                         <td>' + result['resultset'][i].is_pure_breed + '</td>\
                        <td>' + result['resultset'][i].is_adopted + '</td>\
                        <td>' + result['resultset'][i].is_for_adoption + '</td>\
                        <td><a href="' + base_url + 'Adoption/index/' + result['resultset'][i].id + '" class="" target="_blank">' + result['resultset'][i].requests + '</a></td>\
                        <td>' + result['resultset'][i].createddate + '</td>\
                        <td><a href="#" data-toggle="modal" data-target="#cnf_delete_adp_pet" data-adp-pet-id="' + result['resultset'][i].id + '" class="glyphicon glyphicon-trash trash-icon"></a></td>\
                    </tr>';
        }

        $('.panel.adopt-panel table tbody.adoption-listing').append(adp_pet_html);

        if (offset > 0) {
            cb(true);
        }
    });
}
/* END Adoption Pet Details */

/* START Adoption Pet Delete modal box show */
$("#cnf_delete_adp_pet").on('show.bs.modal', function (event) {
    var petid = $(event.relatedTarget).attr('data-adp-pet-id');
    $(this).find('#btn_del_adp_pet_cnf').attr('data-adp-pet-id', petid);
});
/* END Event Delete modal box show */

/* START Confirm Delete Adoption Pet */
$("#cnf_delete_adp_pet #btn_del_adp_pet_cnf").on('click', function (e) {
    var id = $(this).attr('data-adp-pet-id');
    var del_adp_param = {id: id}
    ajax_call(base_url + 'Adoption/deleteAdoptionPet', 'POST', del_adp_param, function (result) {
        result = JSON.parse(result)

        if (result.status) {
            show_error(result.message, true);
            $("tr.adp_" + id).addClass('animated bounceOutLeft').slideUp(500);
            $('#cnf_delete_adp_pet [data-dismiss="modal"]').trigger('click');
        } else {
            show_error(result.message, false);
        }
    });
});
/* END Confirm Delete Adoption Pet */

/* START ADOPTION INTEREST modal box show */
$("#cnf_pets_interest").on('show.bs.modal', function (event) {
    var id = $(event.relatedTarget).attr('data-adp-owner-id');
    var param = {id: id}
    ajax_call(base_url + 'Adoption/getFavoritePets', 'POST', param, function (result) {

        result = JSON.parse(result);
        var adp_fav_html = '';
        var img;

        if (result['resultset'].length == 0) {
            adp_fav_html = '<tr><td class="text-center">No records found</td></tr>';
            $('.adp-fav-listing').html(adp_fav_html);
        }

        $('table tbody.adp-fav-listing').empty();

        for (var i in result['resultset']) {

            img = result['resultset'][i].userimg ? "" : defaultimg;

            adp_fav_html += '<tr>\
                        <td>' + result['resultset'][i].ownername + '</td>\
                        <td><img src=' + img + ' height="50" width="50"/></td>\
                        <td>' + result['resultset'][i].petname + '</td>\
                        <td><img src=' + result['resultset'][i].petimg + ' height="50" width="50"/></td>\
                    </tr>';

        }

        $('table tbody.adp-fav-listing').append(adp_fav_html);

    });

});
/* END ADOPTION INTEREST modal box show */

/* START EMERGENCY LISTING */
function getEmergencyListing(search, status, fromDate, toDate, offset, cb) {
    var param = {search: search, status: status, fromDate, toDate, offset: offset}
    var emergency_html = '';

    ajax_call(base_url + 'Emergency/getEmergencyListing', 'POST', param, function (result) {

        result = JSON.parse(result);

        if (result['is_available'] == '0') {
            $(".load-more-emergency").parents('.panel-footer').hide();
        }

        if (result['resultset'].length == 0) {
            emergency_html = '<tr><td colspan="6" class="text-center"><b>NO EMERGENCIES FOUND</b></td></tr>';
            $('.panel.adopt-panel table tbody.emergency-listing').html(emergency_html);
        }


        $('.panel.adopt-panel table tbody.emergency-listing').empty();
        for (var i in result['resultset']) {
            var isselected = (result['resultset'][i].status == 1) ? 'selected="selected"' : "";

            emergency_html += '<tr class="adp_' + result['resultset'][i].id + '">\
                        <td>' + result['resultset'][i].description + '</td>\
                        <td>' + result['resultset'][i].name + '</td>\
                        <td>' + result['resultset'][i].location + '</td>\
                        <td>\
                            <select class="dropdown chng_sts" data-id=' + result['resultset'][i].id + ' id = "emergency_status_' + result['resultset'][i].id + '">\
                                <option value="0" ' + isselected + '>Open</option>\
                                <option value="1" ' + isselected + '>Closed</option>\
                            </select>\
                        </td>\
                        <td>' + result['resultset'][i].createddate + '</td>\
                        <td><a href="#" data-toggle="modal" data-target="#cnf_delete_emergency" data-emergency-id="' + result['resultset'][i].id + '" class="glyphicon glyphicon-trash trash-icon"></a></td>\
                    </tr>';
        }

        $('.panel.adopt-panel table tbody.emergency-listing').append(emergency_html);

        if (offset > 0) {
            cb(true);
        }
    });
}
/* END EMERGENCY LISTING */

/* START EMERGENCY DELETE MODAL BOX SHOW */

$("#cnf_delete_emergency").on('show.bs.modal', function (event) {
    var petid = $(event.relatedTarget).attr('data-emergency-id');
    $(this).find('#btn_del_emergency_cnf').attr('data-emergency-id', petid);
});

/* END EMERGENCY DELETE MODAL BOX SHOW */

/* START CONFIRM DELETE EMERGENCY */
$("#cnf_delete_emergency #btn_del_emergency_cnf").on('click', function (e) {
    var id = $(this).attr('data-emergency-id');
    var del_adp_param = {id: id}
    ajax_call(base_url + 'Emergency/deleteEmergency', 'POST', del_adp_param, function (result) {
        result = JSON.parse(result)

        if (result.status) {
            show_error(result.message, true);
            $("tr.adp_" + id).addClass('animated bounceOutLeft').slideUp(500);
            $('#cnf_delete_emergency [data-dismiss="modal"]').trigger('click');
        } else {
            show_error(result.message, false);
        }
    });
});
/* END CONFIRM DELETE EMERGENCY */


/* START CHANGE EMERGENCY STATUS */
$(document).on('change', '.chng_sts', function (e) {

    var id = $(this).attr("data-id");
    var status = $("#emergency_status_" + id).val();
    var del_adp_param = {id: id, status: status}
    ajax_call(base_url + 'Emergency/changeEmergencyStatus', 'POST', del_adp_param, function (result) {
        result = JSON.parse(result);

        if (result.status) {
            show_error(result.message, true);
        } else {
            show_error(result.message, false);
        }
    });
});
/* END CHANGE EMERGENCY STATUS */

/* Load DM Owner Details */
function getDMOwnerDetails(id, offset, cb) {

    var param = {id: id, offset: offset}
    var adp_owner_html = '';
    var img;

    ajax_call(base_url + 'DateMate/getOwnerDetails', 'POST', param, function (result) {
        result = JSON.parse(result);

        if (result['is_available'] == '0') {
            $(".load-more-adoption").parents('.panel-footer').hide();
        }

        if (result['resultset'].length == 0) {
            adp_owner_html = '<div class="text-center">No records found</div>';
            $('.panel.adopt-panel .panel-body table').hide();
            $('.panel.adopt-panel .panel-body').html(adp_owner_html);
        }

        for (var i in result['resultset']) {

            adp_owner_html += '<tr class="adp_' + result['resultset'][i].id + '">\
                        <td>' + result['resultset'][i].name + '</td>\
                        <td>' + result['resultset'][i].gender + '</td>\
                        <td>' + result['resultset'][i].location + '</td>\
                        <td>' + result['resultset'][i].description + '</td>\
                        <td><a href="' + base_url + 'DateMate/adoptionpets/' + result['resultset'][i].id + '" class="" target="_blank">' + result['resultset'][i].pets + '</a></td>\
                        <td><a href="#" data-toggle="modal" data-target="#cnf_pets_interest" data-adp-owner-id="' + result['resultset'][i].id + '">' + result['resultset'][i].interests + '</a></td>\
                        <td>' + result['resultset'][i].createddate + '</td>\
                        <td><a href="#" data-toggle="modal" data-target="#cnf_delete_adp_owner" data-adp-owner-id="' + result['resultset'][i].id + '" class="glyphicon glyphicon-trash trash-icon"></a></td>\
                    </tr>';
        }

        $('.panel.adopt-panel table tbody.adoption-listing').append(adp_owner_html);

        if (offset > 0) {
            cb(true);
        }
    });
}
/* End DM Owner Details */

/* START DM Pet Details */
function getDMPetDetails(id, offset, cb) {
    var param = {id: id, offset: offset}
    var adp_pet_html = '';
    var img;

    ajax_call(base_url + 'DateMate/getPetDetails', 'POST', param, function (result) {
        result = JSON.parse(result);

        if (result['is_available'] == '0') {
            $(".load-more-adoption").parents('.panel-footer').hide();
        }

        if (result['resultset'].length == 0) {
            adp_pet_html = '<div class="text-center">No records found</div>';
            $('.panel.adopt-panel .panel-body table').hide();
            $('.panel.adopt-panel .panel-body').html(adp_pet_html);
        }

        for (var i in result['resultset']) {

            adp_pet_html += '<tr class="adp_' + result['resultset'][i].id + '">\
                        <td>' + result['resultset'][i].petname + '</td>\
                        <td>' + result['resultset'][i].gender + '</td>\
                        <td>' + result['resultset'][i].dob + '</td>\
                        <td>' + result['resultset'][i].category + '</td>\
                        <td>' + result['resultset'][i].breed + '</td>\
                         <td>' + result['resultset'][i].is_pure_breed + '</td>\
                        <td><a href="' + base_url + 'DateMate/index/' + result['resultset'][i].id + '" class="" target="_blank">' + result['resultset'][i].requests + '</a></td>\
                        <td>' + result['resultset'][i].createddate + '</td>\
                        <td><a href="#" data-toggle="modal" data-target="#cnf_delete_adp_pet" data-adp-pet-id="' + result['resultset'][i].id + '" class="glyphicon glyphicon-trash trash-icon"></a></td>\
                    </tr>';
        }

        $('.panel.adopt-panel table tbody.adoption-listing').append(adp_pet_html);

        if (offset > 0) {
            cb(true);
        }
    });
}
/* END DM Pet Details */


/* START SEARCH FILTER */

// ADOPTION OWNER FILTER START //
$(document).on('keyup', '#adp-search', function (e) {
    search_adp_owner();
});

$(document).on('change', '#ddlAdpOwnerGender', function (e) {
    search_adp_owner();
});

$(document).on('click', '#adp-date-filter', function (e) {
    search_adp_owner();
});


function search_adp_owner() {
    var txtsearch = $("#adp-search").val();
    var gender = $("#ddlAdpOwnerGender").val();
    var fromDate = $("#adp-frm").val();
    var toDate = $("#adp-to").val();

    var offset = 0;
    getAdoptionOwnerDetails(txtsearch, gender, fromDate, toDate, 0, offset, function () {
        setTimeout(function () {
            show_loader(panel);
        }, 500);
    });
}

// ADOPTION OWNER FILTER END //

// ADOPTION PET FILTER START //

$(document).on('keyup', '#adp-pets-search', function (e) {
    search_adp_pet();
});

$(document).on('change', '#ddlAdpPetGender', function (e) {
    search_adp_pet();
});

$(document).on('click', '#adp-pet-date-filter', function (e) {
    search_adp_pet();
});

function search_adp_pet() {
    var offset = 0;
    var id = urlid[urlid.length - 1];
    var txtsearch = $("#adp-pets-search").val();
    var gender = $("#ddlAdpPetGender").val();
    var fromDate = $("#adp-pet-frm").val();
    var toDate = $("#adp-pet-to").val();
    getAdoptionPetDetails(txtsearch, gender, fromDate, toDate, id, offset, function () {
        setTimeout(function () {
            show_loader(panel);
        }, 500);
    });
}

// ADOPTION PET FILTER END //

// EMERGENCY FILTER START //
$(document).on('keyup', '#emr-search', function (e) {
    search_emergency();
});

$(document).on('change', '#ddlEmergencyStatus', function (e) {
    search_emergency();
});

$(document).on('click', '#emr-date-filter', function (e) {
    search_emergency();
});


function search_emergency() {
    var offset = 0;
    var id = urlid[urlid.length - 1];
    var txtsearch = $("#emr-search").val();
    var status = $("#ddlEmergencyStatus").val();
    var fromDate = $("#emr-frm").val();
    var toDate = $("#emr-to").val();
    getEmergencyListing(txtsearch, status, fromDate, toDate, offset, function () {
        setTimeout(function () {
            show_loader(panel);
        }, 500);
    });
}
// EMERGENCY FILTER END //

/* END SEARCH FILTER */